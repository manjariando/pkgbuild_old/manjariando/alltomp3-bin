# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=alltomp3-bin
_pkgver=66
_LOver=0.3.19
pkgver=${_LOver}.${_pkgver}
pkgrel=1
arch=('x86_64')
license=('GPL-2.0')
url="https://alltomp3.org"
pkgdesc="Download YouTube videos in MP3 with tags and lyrics"
makedepends=('imagemagick')

source=("${pkgname%-*}_${_LOver}-${_pkgver}_amd64.deb::https://packagecloud.io/AllToMP3/alltomp3/packages/ubuntu/yakkety/${pkgname%-*}_${_LOver}-${_pkgver}_amd64.deb/download.deb"
        "https://metainfo.manjariando.com.br/${pkgname%-*}/com.${pkgname%-*}.metainfo.xml")
sha256sums=('62cfd5a4ca0b8fc9ad8e18c3cda3d6b05ce1957d0c58de20d98067e40f3f09a2'
            '950aef66f3a292b90fdb24a262bd79fe3c4a9152a28b06515f6826dbd9ea3fc9')

package() {
    depends=('chromaprint' 'giflib' 'hicolor-icon-theme' 'lcms2' 'libappindicator-gtk3'
             'libcroco' 'libnotify' 'libvips' 'libxss' 'nss' 'orc' 'python2-pathlib2')
    replaces=("${pkgname%-*}")

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    install -d "${pkgdir}/usr/bin"
    ln -sf '/opt/AllToMP3/alltomp3' "${pkgdir}/usr/bin/alltomp3"

    # Appstream
    install -Dm644 "${pkgdir}/usr/share/icons/hicolor/512x512/apps/${pkgname%-*}.png" "${pkgdir}/usr/share/pixmaps/${pkgname%-*}.png"
    install -Dm644 "${pkgdir}/opt/AllToMP3/resources/app/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname%-*}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgname%-*}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname%-*}.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/${pkgname%-*}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname%-*}.desktop"

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128 256; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/usr/share/icons/hicolor/512x512/apps/${pkgname%-*}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname%-*}.png"
    done
}
